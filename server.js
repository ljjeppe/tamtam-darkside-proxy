﻿var http = require('http'),
    io = require('socket.io')(http),
    request = require('request'),
    concat = require('concat-stream'),
    url = require('url'),
    path = require('path'),
    fs = require('fs');

var PORT = 9000;


io.on('connection', function (socket) {
    console.log('a user connected');
});

var server = http.createServer(function (req, resp) {
    
    var uri = url.parse(req.url).pathname;
    var exists = uri == '/injected.js';
    
    // Requesting our injection file?
    if (!exists) {
        // Nope, do proxying :-)
        write = concat(function (completeResponse) {
            var isBuffer = Buffer.isBuffer(completeResponse);
            
            if (isBuffer) {
                var s = completeResponse.toString('utf8', 0, completeResponse.length);
                //console.log(completeResponse.toString('utf8', 0, completeResponse.length));
                if (s != null && s.indexOf("<html") > -1 && req.url.indexOf(".js") == -1) {
                    completeResponse = modifyResponse(completeResponse);
                }
                resp.end(completeResponse);
            }
            else {
                console.log("No buffer: " + req.url);
                resp.end("");
            }
        });
        
        // Do proxy request
        request({
            'url': req.url
        }, function (error, response, body) {
            //if (!error && response.statusCode == 200) {
            //    var contentType = response.headers['content-type'];
            //}
        }).pipe(write);

    } 
    // Return our little sneaky file
    else {
        
        resp.writeHead(200, { 'Content-Type': 'application/x-javascript' });
        
        var filename = path.join(process.cwd(), uri);
        var fileStream = fs.createReadStream(filename);
        fileStream.pipe(resp);
    }

}).listen(PORT);

function modifyResponse(resp) {
    //console.dir(resp);
    resp = resp + "<script type='text/javascript' src='/injected.js'" + "></" + "script>";
    return resp;
}

console.log('Server listening on port ' + PORT);