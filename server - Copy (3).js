﻿var concat = require('concat-stream');
var http = require('http');
var request = require('request');

http.createServer(function (req, res) {
    write = concat(function (completeResponse) {
        // here is where you can modify the resulting response before passing it back to the client.
        var finalResponse = modifyResponse(completeResponse);
        res.end(finalResponse);
    });
    
    request({
        gzip: false, 
        url: 'http://www.nu.nl'
    }).pipe(write);

}).listen(9000, '0.0.0.0');

function modifyResponse(resp) {
    console.log(resp);
    return resp;
}

console.log('Server is listening');