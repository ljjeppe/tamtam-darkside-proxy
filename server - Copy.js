﻿var http = require('http'),
    httpProxy = require('http-proxy');

//
// Create a proxy server with custom application logic
//
var proxy = httpProxy.createProxyServer({});

proxy.on('proxyReq', function (proxyReq, req, res, options) {
    proxyReq.setHeader('user-agent', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0');
    proxyReq.setHeader('host', options.target.host);
    //console.log(options);
    console.log('REQUESTING', proxyReq.method, proxyReq._headers.host, proxyReq.path);
});

proxy.on('proxyRes', function (proxyRes, req, res) {
    console.log('STATUS', proxyRes.statusCode);
    console.dir(res);
    //console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
});

proxy.on('open', function (proxySocket) {
    console.log('open!');
    // listen for messages coming FROM the target here
    proxySocket.on('data', function () { 
        console.log('data!');
        console.log(arguments);
    });
});

proxy.on('close', function (req, socket, head) {
    // view disconnected websocket connections
    console.log('Client disconnected');
});

var server = http.createServer(function (req, res) {
    // You can define here your custom logic to handle the request
    // and then proxy the request.
    proxy.web(req, res, {
        target: 'https://www.google.nl',
        secure: true,
        xfwd: true,
        hostRewrite: true
    });
});


console.log("listening on port 82")
server.listen(82);